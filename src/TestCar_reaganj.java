package lab2Start;
import java.util.Random;

    /**
     * Note: Some TestCar starter code provided by Dr. Sherri Harms of UNK.
     * This program tests the Car object class by simulating three cars driving
     * home and back to school.
     * @author  John Reagan
     * @version 1.1, 02/23/2021
     */

class TestCar {
    public static void main(String[] args) {
        lab2Start.Car sherriCar = new lab2Start.Car("Dr. Harms",
                90);
        // Get on the interstate at 75 mph
        for (int i = 0; i * 5 < 75; i++)
            sherriCar.accelerate(); //we accelerate 5 mph at a time
        // Drive Dr. Harms to see her kids in Lincoln - 110 minutes at 75 mph
        for (int i = 0; i < 110; i++)
            sherriCar.move(); //move one minute at a time
        // when she gets to the Lincoln city limits, slow her car to 65 mph and
        // then drive for 10 minutes
        for (int i = 0; i < 2; i++)
            sherriCar.brake();
        for (int i = 0; i < 10; i++)
            sherriCar.move();
        // output car information - showing how far she drove
        System.out.println(sherriCar);

        // Create a car for you if you can drive home. Drive your car home and
        // output car info.
        lab2Start.Car johnCar = new lab2Start.Car("John",
                100);
        // Get on the interstate at 80 mph
        for (int i = 0; i * 5 < 80; i++)
            johnCar.accelerate();
        // Drive for 180 minutes until the audio book ends. Vary speed randomly
        // due to speed traps and intermittent road rage.
        Random randomNumber = new Random();  // Create a Random class object.
        for (int time = 0; time < 180; time++) {
            //System.out.println(johnCar.getCurrentSpeed() + " mph");
            int random = randomNumber.nextInt(2);
            // Use a switch statement to determine my erratic driving.
            switch (random) {
                case 0:
                    johnCar.brake();    // another speed trap
                    johnCar.move();
                    break;
                case 1:
                    johnCar.accelerate();   // stupid trucks!
                    johnCar.move();
                    break;
                case 2:
                    johnCar.move();     // possibly normal driving, or not
                    break;
            }
            //System.out.println(johnCar.getDistanceTraveled() + " miles");
        }
        // After smoking for most of the trip, the transmission finally fails.
        // John is forced to continue traveling another indeterminate amount of
        // time in second gear.
        while (johnCar.getCurrentSpeed() > 25)  // car will only go 25mph
            johnCar.brake();
        int carDying = randomNumber.nextInt(60);
        for (int time2 = 0; time2 < carDying; time2++)
            johnCar.move();
        // John's karma continues to amaze, as he runs out of gas.
        while (johnCar.getCurrentSpeed() > 0)
            johnCar.brake();
        // John proceeds to abandon his dead car on the side of the road and
        // output's the horrendous trip info from the Car.
        System.out.println(johnCar);

        /* Assignment - create a car for a second CSIT 150 student
         * I'm going to replicate my car's trip code above, to show that the
         * Random number generator and switch statement are producing
         * non-replicable, unique results.
         */
        lab2Start.Car bozoCar = new lab2Start.Car("Bozo",
                100);
        // Get on the interstate at 80 mph.
        for (int i = 0; i * 5 < 80; i++)
            bozoCar.accelerate();
        // Drive for 180 minutes, varying the car speed.
        for (int time = 0; time < 180; time++) {
            int random2 = randomNumber.nextInt(2);
            // Use a switch statement to vary car behavior.
            switch (random2) {
                case 0:
                    bozoCar.brake();    // another speed trap
                    bozoCar.move();
                    break;
                case 1:
                    bozoCar.accelerate();   // stupid trucks!
                    bozoCar.move();
                    break;
                case 2:
                    bozoCar.move();     // possibly normal driving
                    break;
            }
        }
        while (bozoCar.getCurrentSpeed() > 25)  // car will only go 25mph
            bozoCar.brake();
        int carDying2 = randomNumber.nextInt(60);
        for (int time2 = 0; time2 < carDying2; time2++)
            bozoCar.move();
        while (bozoCar.getCurrentSpeed() > 0)
            bozoCar.brake();
        System.out.println(bozoCar);

        //check to see who drove the farthest (in code)
        String farthestOwner;
        double farthestDist;
        double dist1 = sherriCar.getDistanceTraveled();
        double dist2 = johnCar.getDistanceTraveled();
        double dist3 = bozoCar.getDistanceTraveled();
        if (dist1>dist2 && dist1>dist3) {
            farthestOwner = sherriCar.getOwner();
            farthestDist = dist1;
        }
        else if (dist2>dist1 && dist2>dist3) {
            farthestOwner = johnCar.getOwner();
            farthestDist = dist2;
        }
        else {
            farthestOwner = bozoCar.getOwner();
            farthestDist = dist3;
        }
        //output the car that drove the farthest
        System.out.printf("\n%s's car drove the farthest: %.2f miles\n",
                farthestOwner, farthestDist);

        // output the car that drove the longest amount of time
        String longestOwner;
        int longestTripTime;
        int trip1 = sherriCar.getTripTime();
        int trip2 = johnCar.getTripTime();
        int trip3 = bozoCar.getTripTime();
        if (trip1>trip2 && trip1>trip3) {
            longestOwner = sherriCar.getOwner();
            longestTripTime = trip1;
        }
        else if (trip2>trip1 && trip2>trip3) {
            longestOwner = johnCar.getOwner();
            longestTripTime = trip2;
        }
        else {
            longestOwner =  bozoCar.getOwner();
            longestTripTime = trip3;
        }
        System.out.printf("%s's car drove the longest amount of time: %d " +
                "minutes\n", longestOwner, longestTripTime);

        // Bonus: reset each car's distance traveled. trip time, and speed, and
        // drive them back to Kearney.
        // John and Bozo's cars are dead, so I'll just drive Sheri's car back.
        sherriCar.setDistanceTraveled(0);
        sherriCar.setTripTime(0);
        sherriCar.setCurrentSpeed(0);

        // Accelerate to 65 mph until outside the Lincoln city limits in 10 min.
        for (int i = 0; i * 5 < 65; i++)
            sherriCar.accelerate(); //we accelerate 5 mph at a time
        for (int i = 0; i < 10; i++)
            sherriCar.move();
        // Outside of Lincoln, accelerate to 75 mph for the 110 minute trip back
        // to Kearney.
        for (int i = 0; i < 2; i++)
            sherriCar.accelerate();
        for (int i = 0; i < 110; i++)
            sherriCar.move(); //move one minute at a time
        // Stop at the destination
        while (sherriCar.getCurrentSpeed() > 0) {
            sherriCar.brake();
        }
        // output car information - showing how far she drove
        System.out.println("\nReturn trip to Kearney... notice the same " +
                "identical results as the departure trip,\nconfirming the " +
                "mutator methods work.");
        System.out.println(sherriCar);

        //Bonus: test the car's copy constructor
        lab2Start.Car sherriTwin = new lab2Start.Car(sherriCar);
        System.out.println("\nCopy constructor works... new Car has the same " +
                "field values as the Car object it was copied from.");
        System.out.println(sherriTwin);

        // another way to verify different objects
        if (sherriCar == sherriTwin) {
            System.out.println("The sherriCar and sheriTwin variables " +
                    "reference the same Car object.");
        } else {
            System.out.println("sherriCar and sheriTwin reference different " +
                    "Car objects.");
        }
    }
}
