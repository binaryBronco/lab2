package lab2Start;

    /**
     * Note: Some Lab2 starter code provided by Dr. Sherri Harms of UNK.
     * This program represents a car that can accelerate, brake, and move.
     * Associated mutator and accessor methods have been established below.
     * @author  John Reagan
     * @version 1.1, 02/23/2021
     */

class Car {
    private static final int DEFAULTMAXSPEED = 100;
    //declare private member variables here
    private String owner;
    private int currentSpeed;           // in terms of mph
    private double distanceTraveled;    // in terms of miles
    private int tripTime;               // in terms of minutes
    private int maxSpeed;               // in terms of mph

    /**
     * Constructor initializes a car
     * @param newOwner      owner of the car
     * @param newMaxSpeed   max speed of the car
     */
    public Car(String newOwner, int newMaxSpeed) {
        owner = newOwner;
        currentSpeed = 0;
        maxSpeed = newMaxSpeed;
        distanceTraveled = 0.0;
        tripTime = 0;
    }

    /**
     * Default constructor
     */
    public Car() {
        this("no owner", DEFAULTMAXSPEED);
    }

    /**
     * Copy constructor
     * @param anotherCar    another car to be copied
     */
    public Car(Car anotherCar) {
        this.setOwner(anotherCar.getOwner());
        this.setCurrentSpeed(anotherCar.getCurrentSpeed());
        this.setMaxSpeed(anotherCar.getMaxSpeed());
        this.setDistanceTraveled(anotherCar.getDistanceTraveled());
        this.setTripTime(anotherCar.getTripTime());
    }

    /**
     * Move the car one minutes at a time. Update the distanceTraveled and
     * tripTime parameters.
     */
    public void move() {
        //  distance = velocity * time, time = 1 hour / 60 minutes
        setDistanceTraveled(getDistanceTraveled() + getCurrentSpeed()/60.0);
        //  increment tripTime, which logs minutes traveled
        setTripTime(getTripTime()+1);
    }

    /**
     * Add 5 miles per hour to current speed.
     * Remember: The most the current speed can be is the maxSpeed of the car
     */
    public void accelerate() {
        if (getCurrentSpeed()+5 >= getMaxSpeed())
            setCurrentSpeed(getMaxSpeed());         // car is at maxSpeed
        else
            setCurrentSpeed(getCurrentSpeed()+5);
    }

    /**
     * Subtract 5 miles per hour from current speed. The minimum speed is zero.
     */
    public void brake() {
        if (getCurrentSpeed()-5 <= 0)
            setCurrentSpeed(0);                     // car will stop
        else
            setCurrentSpeed(getCurrentSpeed()-5);
    }

    //MUTATOR METHODS
    /**
     * Mutator setCurrentSpeed sets the currentSpeed parameter.
     * @param speed     The car's current speed, in mph.
     */
    public void setCurrentSpeed(int speed) { currentSpeed = speed; }

    /**
     * Mutator setMaxSpeed sets the maxSpeed parameter.
     * @param max       The car's max speed, in mph.
     */
    public void setMaxSpeed(int max) { maxSpeed = max; }

    /**
     * Mutator setDistanceTraveled sets the distanceTraveled parameter.
     * @param dist      The car's distance traveled, in miles.
     */
    public void setDistanceTraveled(double dist) { distanceTraveled = dist; }

    /**
     * Mutator setOwner sets the owner parameter.
     * @param newOwner  The car's new owner.
     */
    public void setOwner(String newOwner) { owner = newOwner; }

    /**
     * Mutator setTripTime sets the tripTime parameter.
     * @param time      The car's trip time, in minutes.
     */
    public void setTripTime(int time) { tripTime = time; }

    // ACCESSOR METHODS
    /**
     * Accessor getCurrentSpeed gets the currentSpeed parameter.
     * @return the current speed of the car
     */
    public int getCurrentSpeed() {
        return currentSpeed;
    }

    /**
     * Accessor getMaxSpeed gets the maxSpeed parameter.
     * @return the max speed of the car
     */
    public int getMaxSpeed() {
        return maxSpeed;
    }

    /**
     * Accessor getDistanceTraveled gets the distanceTraveled parameter.
     * @return  The value of the distanceTraveled field.
     */
    public double getDistanceTraveled() { return distanceTraveled; }

    /**
     * Accessor getOwner gets the owner parameter.
     * @return  The value of the owner field.
     */
    public String getOwner() { return owner; }

    /**
     * Accessor getTripTime gets the tripTime parameter.
     * @return  The value of the tripTime field.
     */
    public int getTripTime() { return tripTime; }

    /**
     * Displays the state of the object.
     * @return the car values as a String
     */
    public String toString() {
        String carValue = owner + "'s car current speed: " + currentSpeed +
                " MPH, distance traveled: " +
                String.format("%8.2f", distanceTraveled) +
                " miles, trip time in minutes: " + tripTime;
        return carValue;
    }
}
